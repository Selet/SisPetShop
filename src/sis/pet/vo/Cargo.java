package sis.pet.vo;

public class Cargo {
	private String funcao;
	private int codCargo;
	
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public String getFuncao() {
		return funcao;
	}
	public int getCodCargo() {
		return codCargo;
	}
	public void setCodCargo(int codCargo) {
		this.codCargo = codCargo;
	}
}
