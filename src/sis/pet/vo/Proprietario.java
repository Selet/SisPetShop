package sis.pet.vo;

public class Proprietario {
	private int codProprietario;
	private Pessoa pessoa;
	private Animal pet;
	private Endereco codEndereco;
	
	public void setCodProprietario(int codProprietario) {
		this.codProprietario = codProprietario;
	}
	public int getCodProprietario() {
		return codProprietario;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPet(Animal pet) {
		this.pet = pet;
	}
	public Animal getPet() {
		return pet;
	}
	public Endereco getCodEndereco() {
		return codEndereco;
	}
	public void setCodEndereco(Endereco codEndereco) {
		this.codEndereco = codEndereco;
	}
}
