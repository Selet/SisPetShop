package sis.pet.vo;

import java.util.Date;

public class Servico {
	private String descricao;
	private int codServico;
	private double valor;
	private Animal pet;
	private String nome;
	private Date date;
		
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	
	public void setCodServico(int codServico) {
		this.codServico = codServico;
	}
	public int getCodServico() {
		return codServico;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	public double getValor() {
		return valor;
	}
	
	public void setPet(Animal pet) {
		this.pet = pet;
	}
	public Animal getPet() {
		return pet;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
