package sis.pet.vo;

public class Pessoa {
	private int codPessoa;
	private String nome;
	private String cpf;
	private String telefone;
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	
	public void setCPF(String cpf) {
		this.cpf = cpf;
	}
	public String getCPF() {
		return cpf;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getTelefone() {
		return telefone;
	}
	public int getCodPessoa() {
		return codPessoa;
	}
	public void setCodPessoa(int codPessoa) {
		this.codPessoa = codPessoa;
	}
}
