package sis.pet.vo;

public class Animal {
	private String nome;
	private int codAnimal;
	private String especie;
	private String raca;
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	
	public void setCodAnimal(int codAnimal) {
		this.codAnimal = codAnimal;
	}
	public int getCodAnimal() {
		return codAnimal;
	}
	
	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getEspecie() {
		return especie;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getRaca() {
		return raca;
	}
}
