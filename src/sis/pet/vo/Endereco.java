package sis.pet.vo;

public class Endereco {
	private int codEndereco;
	private String logradouro;
	private int numCasa;
	private String bairro;
	private String cep;
	private String complemento;
	private String cidade;
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setNumCasa(int numCasa) {
		this.numCasa = numCasa;
	}
	public int getNumCasa() {
		return numCasa;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getBairro() {
		return bairro;
	}
	
	public void setCEP(String cep) {
		this.cep = cep;
	}
	public String getCEP() {
		return cep;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getComplemento() {
		return complemento;
	}
	
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getCidade() {
		return cidade;
	}
	public int getCodEndereco() {
		return codEndereco;
	}
	public void setCodEndereco(int codEndereco) {
		this.codEndereco = codEndereco;
	}
}
