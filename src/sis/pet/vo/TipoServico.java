package sis.pet.vo;

public class TipoServico {
	private int codTipoServico;
	private String nome;
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public int getCodTipoServico() {
		return codTipoServico;
	}
	public void setCodTipoServico(int codTipoServico) {
		this.codTipoServico = codTipoServico;
	}
}
