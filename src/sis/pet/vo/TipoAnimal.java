package sis.pet.vo;

public class TipoAnimal {
	private int idTipoAnimal;
	private String especie;
	private String raca;

	public int getIdTipoAnimal() {
		return idTipoAnimal;
	}

	public void setIdTipoAnimal(int id) {
		this.idTipoAnimal = id;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getEspecie() {
		return especie;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getRaca() {
		return raca;
	}
}
