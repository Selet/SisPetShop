package sis.pet.gui;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sis.pet.dao.AnimalDAO;
import sis.pet.vo.Animal;

@Controller
public class AnimalController {
	
	@Autowired
	AnimalDAO animalDAO;
	
	@RequestMapping(value="/animal" , method = RequestMethod.GET)
	public String index(Model model) {

		List<Animal> listAnimal = animalDAO.list();
		model.addAttribute("animal", listAnimal);
		return "/animal/lista";
	}
}

