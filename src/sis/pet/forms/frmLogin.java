package sis.pet.forms;

import javax.swing.JOptionPane;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class frmLogin extends Application {
	private GridPane gpane;
	private TextField txtLogin;
	private PasswordField txtSenha;
	private Button btnEntrar, btnSair;
	private static Stage stage;
	private Label lbLogin, lbSenha;
	private HBox hboxBotoes, hboxImagens;

	@Override
	public void start(Stage stage) throws Exception {
		initGridPane();
		initListeners();
		Scene scene = new Scene(gpane, 500, 500);
		stage.setScene(scene);
		stage.setResizable(false);
		scene.getStylesheets().add("login.css");
		stage.setTitle("SISTEMA PET SHOP - SisPet");
		stage.show();
	}

	// M�todo com a configura��o do painel e componentes
	public void initGridPane() {
		// Configura��o do Painel
		gpane = new GridPane();
		gpane.setAlignment(Pos.CENTER);
		gpane.setPadding(new Insets(25, 25, 25, 25));
		gpane.setHgap(10);
		gpane.setVgap(10);

		hboxImagens = new HBox();
		Image img = new Image("logo_cachorro.png");
		ImageView logo = new ImageView(img);
		Image img2 = new Image("pegadas.png");
		ImageView imgPegadas = new ImageView(img2);
		hboxImagens.getChildren().addAll(logo);
		gpane.add(hboxImagens, 1, 0);

		// Adi��o dos componetes
		lbLogin = new Label("Usu�rio:");
		lbLogin.getStyleClass().add("labelsLogin");
		gpane.add(lbLogin, 1, 1);// (componente, coluna, linha)
		txtLogin = new TextField();
		txtLogin.setPrefSize(250, 20);
		gpane.add(txtLogin, 1, 2);
		lbSenha = new Label("Senha:");
		lbSenha.getStyleClass().add("labelsLogin");
		gpane.add(lbSenha, 1, 3);
		txtSenha = new PasswordField();
		txtSenha.setPrefSize(250, 20);
		gpane.add(txtSenha, 1, 4);

		btnEntrar = new Button("Entrar");
		btnEntrar.setPrefSize(75, 20);// largura e altura do bot�o
		btnEntrar.getStyleClass().add("btnEntrar");// Configura��o de estilo no bot�o

		btnSair = new Button("Sair");
		btnSair.getStyleClass().add("btnSair");// Configura��o de estilo no bot�o
		btnSair.setPrefSize(75, 20);// largura e altura do bot�o
		hboxBotoes = new HBox();// Adiciona os componentes na horizontal
		// Espa�o entre os componentes
		hboxBotoes.setSpacing(20);
		hboxBotoes.getChildren().addAll(btnEntrar, btnSair);
		gpane.add(hboxBotoes, 1, 6);
		gpane.getStyleClass().add("pane");

	}

	// M�todo de a��es dos bot�es
	public void initListeners() {
		btnSair.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				fecharAplicacao();
			}
		});

		btnEntrar.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				logar();
			}
		});
	}

	private void logar() {
		if (txtLogin.getText().equals("admin") && txtSenha.getText().equals("senhafacil")) {
			JOptionPane.showMessageDialog(null, "Login realizado com sucesso!!!", "Sucesso",
					JOptionPane.INFORMATION_MESSAGE);
			fecharAplicacao();
		} else {
			JOptionPane.showMessageDialog(null, "Login e/ou senha inv�lidos", "Erro", JOptionPane.ERROR_MESSAGE);
			txtLogin.setText("");
			txtSenha.setText("");
		}
	}

	public void fecharAplicacao() {
		System.exit(0);
	}

	public static Stage getStage() {
		return stage;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
