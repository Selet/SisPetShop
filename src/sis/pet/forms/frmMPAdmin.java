package sis.pet.forms;

import java.nio.BufferUnderflowException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class frmMPAdmin extends Application{
	
	private AnchorPane anchorP;
	private HBox hboxMenuBotoes, hboxLogo;
	private Button btnFuncionario, btnAnimal, btnProprietario, btnServico;
	
	@Override
	public void start(Stage stage) throws Exception {
		
		anchorP = new AnchorPane();
		Scene scene = new Scene(anchorP, 1350, 690);
		anchorP.getStyleClass().add("anchorP");
		anchorP.getChildren().addAll(hboxLogo,hboxMenuBotoes);
		stage.setScene(scene);
		stage.show();

		addHboxElements();
		// stage.setResizable(false);
		scene.getStylesheets().add("frmMenuPrincipal.css");
		stage.setTitle("SISTEMA PET SHOP - SisPet");
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private void addHboxElements() {
		hboxLogo = new HBox();
		hboxMenuBotoes = new HBox();
		
		btnAnimal = new Button();
		btnFuncionario = new Button();
		btnServico = new Button();
		btnProprietario = new Button();

	}

}
