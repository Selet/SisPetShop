package sis.pet.forms;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class frmCadastrarAnimal extends Application {
	private AnchorPane anchorP;
	private GridPane gpane;
	private HBox hboxBotoes, hboxMenuSuperior, hboxRodape, hboxEl4Txt1, hboxEl4Txt2, hboxEl4Txt3;
	private Button btnSalvar, btnCancelar, btnVoltar, btnMenu, btnSair;
	private Label lbTituloMenu, lbLogado, lbPetShop, lbCodAnimal, lbNomeAnimal, lbEspecieAnimal;
	private VBox vboxEl3, vboxEl4;
	private TextField txtCodAnimal, txtNomeAnimal, txtEspecieAnimal;

	@Override
	public void start(Stage stage) throws Exception {
		anchorP = new AnchorPane();
		Scene scene = new Scene(anchorP, 1350, 690);
		anchorP.getStyleClass().add("anchorP");
		stage.setScene(scene);
		stage.show();

		addHboxMenuSuperior();
		addHboxButtons();
		addHboxRodape();
		addVboxElements();
		
		scene.getStylesheets().add("frmCadastrarAnimal.css");
		stage.setTitle("SISTEMA PET SHOP - SisPet");
		anchorP.getChildren().addAll(gpane, hboxMenuSuperior,vboxEl3, vboxEl4, hboxRodape);

	}

	private void addHboxMenuSuperior() {
		btnVoltar = new Button();
		btnVoltar.setPrefSize(70, 70);
		btnVoltar.getStyleClass().add("btnVoltar");
		btnVoltar.setPadding(new Insets(10, 0, 0, 0));// top,right,bottom,left

		btnMenu = new Button();
		btnMenu.setPrefSize(70, 70);
		btnMenu.getStyleClass().add("btnMenu");

		Image imgpatinhas = new Image("img2patinhas.png");
		ImageView patinhas = new ImageView(imgpatinhas);

		Image imgpatinhas2 = new Image("img2patinhas.png");
		ImageView patinhas2 = new ImageView(imgpatinhas2);

		lbTituloMenu = new Label("Animal");
		lbTituloMenu.getStyleClass().add("lbTituloMenu");

		btnSair = new Button();
		btnSair.setPrefSize(70, 70);
		btnSair.getStyleClass().add("btnSair");
		btnSair.setAlignment(Pos.CENTER_RIGHT);

		hboxMenuSuperior = new HBox();
		hboxMenuSuperior.setPrefSize(900, 70);
		hboxMenuSuperior.setLayoutX(25);
		hboxMenuSuperior.setLayoutY(30);
		hboxMenuSuperior.setSpacing(20);
		hboxMenuSuperior.getStyleClass().add("hboxMenuSuperior");
		hboxMenuSuperior.getChildren().addAll(btnVoltar, btnMenu, patinhas, lbTituloMenu, patinhas2, btnSair);
	}

	private void addVboxElements() {

		vboxEl3 = new VBox();
		vboxEl3.setPrefSize(100, 500);
		vboxEl3.getStyleClass().add("vboxEl3");
		vboxEl3.setLayoutX(25);
		vboxEl3.setLayoutY(150);
		vboxEl3.setSpacing(18.5);
		vboxEl3.setPadding(new Insets(40, 0, 0, 20));
		addLabelsEl3();
		vboxEl3.getChildren().addAll(lbCodAnimal, lbNomeAnimal, lbEspecieAnimal);

		vboxEl4 = new VBox();
		vboxEl4.setPrefSize(800, 500);
		vboxEl4.getStyleClass().add("vboxEl4");
		vboxEl4.setLayoutX(125);
		vboxEl4.setLayoutY(150);
		vboxEl4.setSpacing(10);
		vboxEl4.setPadding(new Insets(40, 20, 0, 20));
		addTextFieldsEl4();
		vboxEl4.getChildren().addAll(hboxEl4Txt1, hboxEl4Txt2, hboxEl4Txt3);

	}

	private void addLabelsEl3() {
		lbCodAnimal = new Label("C�d. Animal:");
		lbNomeAnimal = new Label("Nome do Pet:");
		lbEspecieAnimal = new Label("Esp�cie:");

	}
	
	private void addTextFieldsEl4() {
		hboxEl4Txt1 = new HBox();
		hboxEl4Txt2 = new HBox();
		hboxEl4Txt3 = new HBox();

		txtCodAnimal = new TextField();
		txtNomeAnimal = new TextField();
		txtEspecieAnimal = new TextField();

		txtCodAnimal.setPrefSize(70, 10);

		hboxEl4Txt2.getChildren().addAll(txtNomeAnimal);
		hboxEl4Txt1.getChildren().addAll(txtCodAnimal);
		hboxEl4Txt3.getChildren().addAll(txtEspecieAnimal);

	}
	
	public void addHboxButtons() {
		gpane = new GridPane();
		hboxBotoes = new HBox();
		btnSalvar = new Button("Salvar");
		btnSalvar.setPrefSize(75, 30);
		btnCancelar = new Button("Cancelar");
		btnCancelar.setPrefSize(75, 30);
		gpane.getStyleClass().add("hboxBotoes");
		hboxBotoes.setSpacing(20);
		gpane.setHgap(10);
		gpane.setVgap(10);
		gpane.add(hboxBotoes, 1, 1);
		gpane.setPrefSize(900, 50);// (largura/altura)
		gpane.setLayoutX(25);
		gpane.setLayoutY(100);
		hboxBotoes.getChildren().addAll(btnSalvar, btnCancelar);

	}

	private void addHboxRodape() {
		lbLogado = new Label("Usu�rio:");
		lbLogado.setPadding(new Insets(0, 0, 0, 10));// top,right,bottom,left
		lbPetShop = new Label("Sistema PetShop - Vers�o 1.0");
		lbPetShop.getStyleClass().add("lbPetShop");
		hboxRodape = new HBox();
		hboxRodape.setSpacing(650);
		hboxRodape.setPrefSize(900, 20);
		hboxRodape.setLayoutX(25);
		hboxRodape.setLayoutY(675);
		hboxRodape.getChildren().addAll(lbLogado, lbPetShop);
		hboxRodape.getStyleClass().add("hboxRodape");
	}

	public static void main(String[] args) {
		launch(args);
	}

}
