package sis.pet.forms;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class frmCadastrarProprietario extends Application {
	private GridPane gpane;
	private AnchorPane anchorP;
	private TextField txtNome, txtCPF, txtTelefone, txtLogradouro, txtNumCasa, txtBairro, txtCEP, txtComplemento,
			txtCidade, txtCodAnimal, txtNomeAnimal, txtEspecieAnimal;
	private Label lbNome, lbCPF, lbTelefone, lbLogradouro, lbNumCasa, lbBairro, lbCEP, lbComplemento, lbCidade,
			lbCodAnimal, lbNomeAnimal, lbEspecieAnimal, lbLogado, lbPetShop,lbTituloMenu;
	private HBox hboxBotoes, hboxEl2txt1, hboxEl2txt2, hboxEl2txt3, hboxEl2txt4, hboxEl2txt5, hboxEl2txt6, hboxEl2txt7,
			hboxEl2txt8, hboxEl2txt9, hboxEl4Txt1, hboxEl4Txt2, hboxEl4Txt3, hboxRodape, hboxMenuSuperior;
	private Button btnSalvar, btnCancelar, btnVoltar, btnSair, btnMenu;
	private static Stage stage;
	private VBox vboxEl1, vboxEl2, vboxEl3, vboxEl4;
	

	@Override
	public void start(Stage stage) throws Exception {
		anchorP = new AnchorPane();
		Scene scene = new Scene(anchorP, 1350, 690);
		anchorP.getStyleClass().add("anchorP");
		stage.setScene(scene);
		stage.show();

		 
		addHboxButtons();
		addVboxElements();
		addHboxRodape();
		addHboxMenuSuperior();
		initListeners();
		// stage.setResizable(false);
		scene.getStylesheets().add("frmCadastrarProprietario.css");
		stage.setTitle("SISTEMA PET SHOP - SisPet");
		anchorP.getChildren().addAll(gpane, vboxEl1, vboxEl2, vboxEl3, vboxEl4, hboxRodape, hboxMenuSuperior);

	}

	private void addHboxMenuSuperior() {
		btnVoltar = new Button();
		btnVoltar.setPrefSize(70, 70);
		btnVoltar.getStyleClass().add("btnVoltar");
		btnVoltar.setPadding(new Insets(10, 0, 0, 0));// top,right,bottom,left
		
		btnMenu = new Button();
		btnMenu.setPrefSize(70, 70);
		btnMenu.getStyleClass().add("btnMenu");
		
		Image imgpatinhas = new Image("img2patinhas.png");
		ImageView patinhas = new ImageView(imgpatinhas);
		
		Image imgpatinhas2 = new Image("img2patinhas.png");
		ImageView patinhas2 = new ImageView(imgpatinhas2);
		
		lbTituloMenu = new Label("Propriet�rio");
		lbTituloMenu.getStyleClass().add("lbTituloMenu");
		
		btnSair = new Button();
		btnSair.setPrefSize(70, 70);
		btnSair.getStyleClass().add("btnSair");
		btnSair.setAlignment(Pos.CENTER_RIGHT);
		
		hboxMenuSuperior = new HBox();
		hboxMenuSuperior.setPrefSize(900, 70);
		hboxMenuSuperior.setLayoutX(25);
		hboxMenuSuperior.setLayoutY(30);
		hboxMenuSuperior.setSpacing(20);
		hboxMenuSuperior.getStyleClass().add("hboxMenuSuperior");
		hboxMenuSuperior.getChildren().addAll(btnVoltar,btnMenu,patinhas,lbTituloMenu,patinhas2,btnSair);
	}

	private void addHboxRodape() {
		lbLogado = new Label("Usu�rio:");
		lbLogado.setPadding(new Insets(0, 0, 0, 10));// top,right,bottom,left
		lbPetShop = new Label("Sistema PetShop - Vers�o 1.0");
		lbPetShop.getStyleClass().add("lbPetShop");
		hboxRodape = new HBox();
		hboxRodape.setSpacing(650);
		hboxRodape.setPrefSize(900, 20);
		hboxRodape.setLayoutX(25);
		hboxRodape.setLayoutY(675);
		hboxRodape.getChildren().addAll(lbLogado, lbPetShop);
		hboxRodape.getStyleClass().add("hboxRodape");
	}

	private void addVboxElements() {
		vboxEl1 = new VBox();
		vboxEl1.setPrefSize(100, 500);// (largura/altura)
		vboxEl1.getStyleClass().add("vboxEl1");
		vboxEl1.setLayoutX(25);
		vboxEl1.setLayoutY(150);
		vboxEl1.setSpacing(18.5);
		vboxEl1.setPadding(new Insets(40, 0, 0, 20));// top,right,bottom,left
		addLabelsEl1();
		vboxEl1.getChildren().addAll(lbNome, lbCPF, lbTelefone, lbLogradouro, lbNumCasa, lbBairro, lbCEP, lbComplemento,
				lbCidade);

		vboxEl2 = new VBox();
		vboxEl2.setPrefSize(400, 500);// (largura/altura)
		vboxEl2.getStyleClass().add("vboxEl2");
		vboxEl2.setLayoutX(125);
		vboxEl2.setLayoutY(150);
		vboxEl2.setSpacing(10);
		vboxEl2.setPadding(new Insets(40, 20, 0, 20));
		addTextFieldsEl2();
		vboxEl2.getChildren().addAll(hboxEl2txt1, hboxEl2txt2, hboxEl2txt3, hboxEl2txt4, hboxEl2txt5, hboxEl2txt6,
				hboxEl2txt7, hboxEl2txt8, hboxEl2txt9);

		vboxEl3 = new VBox();
		vboxEl3.setPrefSize(100, 500);
		vboxEl3.getStyleClass().add("vboxEl3");
		vboxEl3.setLayoutX(525);
		vboxEl3.setLayoutY(150);
		vboxEl3.setSpacing(18.5);
		vboxEl3.setPadding(new Insets(40, 0, 0, 20));
		addLabelsEl3();
		vboxEl3.getChildren().addAll(lbCodAnimal, lbNomeAnimal, lbEspecieAnimal);

		vboxEl4 = new VBox();
		vboxEl4.setPrefSize(300, 500);
		vboxEl4.getStyleClass().add("vboxEl4");
		vboxEl4.setLayoutX(625);
		vboxEl4.setLayoutY(150);
		vboxEl4.setSpacing(10);
		vboxEl4.setPadding(new Insets(40, 20, 0, 20));
		addTextFieldsEl4();
		vboxEl4.getChildren().addAll(hboxEl4Txt1, hboxEl4Txt2, hboxEl4Txt3);

	}

	public void addLabelsEl1() {
		lbNome = new Label("Nome:");
		lbCPF = new Label("CPF:");
		lbTelefone = new Label("Telefone:");
		lbLogradouro = new Label("Logradouro:");
		lbNumCasa = new Label("N�m. Casa/Apto:");
		lbBairro = new Label("Bairro:");
		lbCEP = new Label("CEP:");
		lbComplemento = new Label("Complemento:");
		lbCidade = new Label("Cidade:");
	}

	private void addLabelsEl3() {
		lbCodAnimal = new Label("C�d. Animal:");
		lbNomeAnimal = new Label("Nome do Pet:");
		lbEspecieAnimal = new Label("Esp�cie:");

	}

	private void addTextFieldsEl2() {
		hboxEl2txt1 = new HBox();
		hboxEl2txt2 = new HBox();
		hboxEl2txt3 = new HBox();
		hboxEl2txt4 = new HBox();
		hboxEl2txt5 = new HBox();
		hboxEl2txt6 = new HBox();
		hboxEl2txt7 = new HBox();
		hboxEl2txt8 = new HBox();
		hboxEl2txt9 = new HBox();

		txtNome = new TextField();
		txtCPF = new TextField();
		txtTelefone = new TextField();
		txtLogradouro = new TextField();
		txtNumCasa = new TextField();
		txtBairro = new TextField();
		txtCEP = new TextField();
		txtComplemento = new TextField();
		txtCidade = new TextField();
		
		Tooltip.install(txtNome, new Tooltip("Digite aqui o nome do Propriet�rio"));
		Tooltip.install(txtCPF, new Tooltip("Digite aqui o CPF do Propriet�rio"));
		Tooltip.install(txtTelefone, new Tooltip("Digite aqui o n�mero para contato"));
		Tooltip.install(txtLogradouro, new Tooltip("Digite aqui o logradouro"));
		Tooltip.install(txtNumCasa, new Tooltip("Digite aqui o n�mero da casa/apartamento"));
		Tooltip.install(txtBairro, new Tooltip("Digite aqui o nome do bairro"));
		Tooltip.install(txtCEP, new Tooltip("Digite aqui o CEP"));
		Tooltip.install(txtComplemento, new Tooltip("Digite aqui o complemento"));
		Tooltip.install(txtCidade, new Tooltip("Digite aqui o nome da Cidade"));

		txtNome.setPrefSize(400, 10);
		txtCPF.setPrefSize(100, 10);
		txtTelefone.setPrefSize(100, 10);
		txtLogradouro.setPrefSize(400, 10);
		txtNumCasa.setPrefSize(50, 10);
		txtBairro.setPrefSize(200, 10);
		txtCEP.setPrefSize(100, 10);
		txtComplemento.setPrefSize(200, 10);
		txtCidade.setPrefSize(200, 10);

		hboxEl2txt1.getChildren().addAll(txtNome);
		hboxEl2txt2.getChildren().addAll(txtCPF);
		hboxEl2txt3.getChildren().addAll(txtTelefone);
		hboxEl2txt4.getChildren().addAll(txtLogradouro);
		hboxEl2txt5.getChildren().addAll(txtNumCasa);
		hboxEl2txt6.getChildren().addAll(txtBairro);
		hboxEl2txt7.getChildren().addAll(txtCEP);
		hboxEl2txt8.getChildren().addAll(txtComplemento);
		hboxEl2txt9.getChildren().addAll(txtCidade);

	}

	private void addTextFieldsEl4() {
		hboxEl4Txt1 = new HBox();
		hboxEl4Txt2 = new HBox();
		hboxEl4Txt3 = new HBox();

		txtCodAnimal = new TextField();
		txtNomeAnimal = new TextField();
		txtEspecieAnimal = new TextField();

		txtCodAnimal.setPrefSize(70, 10);

		hboxEl4Txt2.getChildren().addAll(txtNomeAnimal);
		hboxEl4Txt1.getChildren().addAll(txtCodAnimal);
		hboxEl4Txt3.getChildren().addAll(txtEspecieAnimal);

	}
	// Aplica��o de estilo as labels
	// lbNome.getStyleClass().add("labelsfrmCP");
	/*
	 * lbCPF.getStyleClass().add("labelsfrmCP");
	 * lbTelefone.getStyleClass().add("labelsfrmCP");
	 * lbLogradouro.getStyleClass().add("labelsfrmCP");
	 * lbNumCasa.getStyleClass().add("labelsfrmCP");
	 * lbBairro.getStyleClass().add("labelsfrmCP");
	 * lbCEP.getStyleClass().add("labelsfrmCP");
	 * lbComplemento.getStyleClass().add("labelsfrmCP");
	 * lbCidade.getStyleClass().add("labelsfrmCP");
	 * lbCodAnimal.getStyleClass().add("labelsfrmCP");
	 * lbNomeAnimal.getStyleClass().add("labelsfrmCP");
	 * lbEspecieAnimal.getStyleClass().add("labelsfrmCP");
	 */

	public void addHboxButtons() {
		gpane = new GridPane();
		hboxBotoes = new HBox();
		btnSalvar = new Button("Salvar");
		btnSalvar.setPrefSize(75, 30);
		btnSalvar.getStyleClass().add("btnSalvar");
		btnCancelar = new Button("Cancelar");
		btnCancelar.setPrefSize(75, 30);
		btnCancelar.getStyleClass().add("btnCancelar");
		gpane.getStyleClass().add("hboxBotoes");
		hboxBotoes.setSpacing(20);
		gpane.setHgap(10);
		gpane.setVgap(10);
		gpane.add(hboxBotoes, 1, 1);
		gpane.setPrefSize(900, 50);// (largura/altura)
		gpane.setLayoutX(25);
		gpane.setLayoutY(100);
		hboxBotoes.getChildren().addAll(btnSalvar, btnCancelar);

	}
	
	public void initListeners() {
		btnSair.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				fecharAplicacao();
			}
		});

		/*btnMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				test t = new test();
				try {
					t.start(stage);
				} catch (Exception e) {
					System.out.println("N�o funcionou essa merda");
					
				}
			}
		});*/
	}

	public void fecharAplicacao() {
		System.exit(0);
	}

	public static Stage getStage() {
		return stage;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
