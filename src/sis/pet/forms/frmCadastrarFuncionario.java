package sis.pet.forms;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class frmCadastrarFuncionario extends Application {
	private AnchorPane anchorP;
	private GridPane gpane;
	private HBox hboxBotoes, hboxEl2txt1, hboxEl2txt2, hboxEl2txt3, hboxEl2txt4, hboxEl2txt5, hboxEl2txt6, hboxRodape,
			hboxMenuSuperior;
	private Button btnSalvar, btnCancelar, btnVoltar, btnSair, btnMenu;
	private Label lbNome, lbCPF, lbTelefone, lbCargo, lbSenha, lbConfSenha, lbLogado, lbPetShop, lbTituloMenu;
	private VBox vboxEl1, vboxEl2;
	private TextField txtNome, txtCPF, txtTelefone, txtCargo, txtSenha, txtConfSenha;

	@Override
	public void start(Stage stage) throws Exception {
		anchorP = new AnchorPane();
		Scene scene = new Scene(anchorP, 1350, 690);
		anchorP.getStyleClass().add("anchorP");
		stage.setScene(scene);
		stage.show();

		addHboxButtons();
		addVboxElements();
		addHboxMenuSuperior();
		addHboxRodape();
		scene.getStylesheets().add("frmCadastrarFuncionario.css");
		stage.setTitle("SISTEMA PET SHOP - SisPet");
		anchorP.getChildren().addAll(gpane, vboxEl1, vboxEl2, hboxRodape, hboxMenuSuperior);

	}

	private void addVboxElements() {
		vboxEl1 = new VBox();
		vboxEl1.setPrefSize(150, 500);// (largura/altura)
		vboxEl1.getStyleClass().add("vboxEl1");
		vboxEl1.setLayoutX(25);
		vboxEl1.setLayoutY(150);
		vboxEl1.setSpacing(18.5);
		vboxEl1.setPadding(new Insets(40, 0, 0, 20));// top,right,bottom,left
		addLabelsEl1();
		vboxEl1.getChildren().addAll(lbNome, lbCPF, lbTelefone, lbCargo, lbSenha, lbConfSenha);

		vboxEl2 = new VBox();
		vboxEl2.setPrefSize(800, 500);// (largura/altura)
		vboxEl2.getStyleClass().add("vboxEl2");
		vboxEl2.setLayoutX(125);
		vboxEl2.setLayoutY(150);
		vboxEl2.setSpacing(10);
		vboxEl2.setPadding(new Insets(40, 20, 0, 20));
		addTextFieldsEl2();
		vboxEl2.getChildren().addAll(hboxEl2txt1, hboxEl2txt2, hboxEl2txt3, hboxEl2txt4, hboxEl2txt5, hboxEl2txt6);

	}

	public void addLabelsEl1() {
		lbNome = new Label("Nome:");
		lbCPF = new Label("CPF:");
		lbTelefone = new Label("Telefone:");
		lbCargo = new Label("Cargo:");
		lbSenha = new Label("Senha:");
		lbConfSenha = new Label("Repita a senha:");
	}

	private void addTextFieldsEl2() {
		hboxEl2txt1 = new HBox();
		hboxEl2txt2 = new HBox();
		hboxEl2txt3 = new HBox();
		hboxEl2txt4 = new HBox();
		hboxEl2txt5 = new HBox();
		hboxEl2txt6 = new HBox();

		txtNome = new TextField();
		txtCPF = new TextField();
		txtTelefone = new TextField();
		txtCargo = new TextField();
		txtSenha = new TextField();
		txtConfSenha = new TextField();

		Tooltip.install(txtNome, new Tooltip("Digite aqui o nome do Funcion�rio"));
		Tooltip.install(txtCPF, new Tooltip("Digite aqui o CPF do Funcion�rio"));
		Tooltip.install(txtTelefone, new Tooltip("Digite aqui o n�mero para contato"));
		Tooltip.install(txtSenha, new Tooltip("Digite aqui senha"));
		Tooltip.install(txtConfSenha, new Tooltip("Repita aqui senha"));

		txtNome.setPrefSize(400, 10);
		txtCPF.setPrefSize(100, 10);
		txtTelefone.setPrefSize(100, 10);
		txtSenha.setPrefSize(150, 10);
		txtConfSenha.setPrefSize(150, 10);

		hboxEl2txt1.getChildren().addAll(txtNome);
		hboxEl2txt2.getChildren().addAll(txtCPF);
		hboxEl2txt3.getChildren().addAll(txtTelefone);
		hboxEl2txt4.getChildren().addAll(txtCargo);
		hboxEl2txt5.getChildren().addAll(txtSenha);
		hboxEl2txt6.getChildren().addAll(txtConfSenha);

	}

	public void addHboxButtons() {
		gpane = new GridPane();
		hboxBotoes = new HBox();
		btnSalvar = new Button("Salvar");
		btnSalvar.setPrefSize(75, 30);
		btnCancelar = new Button("Cancelar");
		btnCancelar.setPrefSize(75, 30);
		gpane.getStyleClass().add("hboxBotoes");
		hboxBotoes.setSpacing(20);
		gpane.setHgap(10);
		gpane.setVgap(10);
		gpane.add(hboxBotoes, 1, 1);
		gpane.setPrefSize(900, 50);// (largura/altura)
		gpane.setLayoutX(25);
		gpane.setLayoutY(100);
		hboxBotoes.getChildren().addAll(btnSalvar, btnCancelar);

	}

	private void addHboxMenuSuperior() {
		btnVoltar = new Button();
		btnVoltar.setPrefSize(70, 70);
		btnVoltar.getStyleClass().add("btnVoltar");
		btnVoltar.setPadding(new Insets(10, 0, 0, 0));// top,right,bottom,left

		btnMenu = new Button();
		btnMenu.setPrefSize(70, 70);
		btnMenu.getStyleClass().add("btnMenu");

		Image imgpatinhas = new Image("img2patinhas.png");
		ImageView patinhas = new ImageView(imgpatinhas);

		Image imgpatinhas2 = new Image("img2patinhas.png");
		ImageView patinhas2 = new ImageView(imgpatinhas2);

		lbTituloMenu = new Label("Funcion�rio");
		lbTituloMenu.getStyleClass().add("lbTituloMenu");

		btnSair = new Button();
		btnSair.setPrefSize(70, 70);
		btnSair.getStyleClass().add("btnSair");
		btnSair.setAlignment(Pos.CENTER_RIGHT);

		hboxMenuSuperior = new HBox();
		hboxMenuSuperior.setPrefSize(900, 70);
		hboxMenuSuperior.setLayoutX(25);
		hboxMenuSuperior.setLayoutY(30);
		hboxMenuSuperior.setSpacing(20);
		hboxMenuSuperior.getStyleClass().add("hboxMenuSuperior");
		hboxMenuSuperior.getChildren().addAll(btnVoltar, btnMenu, patinhas, lbTituloMenu, patinhas2, btnSair);
	}

	private void addHboxRodape() {
		lbLogado = new Label("Usu�rio:");
		lbLogado.setPadding(new Insets(0, 0, 0, 10));// top,right,bottom,left
		lbPetShop = new Label("Sistema PetShop - Vers�o 1.0");
		lbPetShop.getStyleClass().add("lbPetShop");
		hboxRodape = new HBox();
		hboxRodape.setSpacing(650);
		hboxRodape.setPrefSize(900, 20);
		hboxRodape.setLayoutX(25);
		hboxRodape.setLayoutY(675);
		hboxRodape.getChildren().addAll(lbLogado, lbPetShop);
		hboxRodape.getStyleClass().add("hboxRodape");
	}

	public static void main(String[] args) {
		launch(args);
	}

}
