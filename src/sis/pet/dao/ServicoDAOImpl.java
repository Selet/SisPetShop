package sis.pet.dao;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.AnimalRowMapper;
import sis.pet.dao.mapper.ServicoRowMapper;
import sis.pet.vo.Animal;
import sis.pet.vo.Servico;

public class ServicoDAOImpl implements ServicoDAO {

	@Autowired
	public JdbcTemplate jdbcTemplate;

	@Override
	public Servico findByCodServico(int codServico) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"servico WHERE codServico =?", new ServicoRowMapper(), codServico);
		} catch (EmptyResultDataAccessException servico) {
			return null;
		}
	}

	@Override
	public Servico findByTipoServico(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM "
					+ "servico WHERE tipoServico =?", new ServicoRowMapper(), nome);
		} catch (EmptyResultDataAccessException servico) {
			return null;
		}
	}

	@Override
	public Servico findByDescricao(String descricao) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"servico WHERE descricao =?", new ServicoRowMapper(), descricao);
		} catch (EmptyResultDataAccessException servico) {
			return null;
		}
	}

	@Override
	public Servico findByValor(double valor) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"servico WHERE valor =?", new ServicoRowMapper(), valor);
		} catch (EmptyResultDataAccessException servico) {
			return null;
		}
	}

	@Override
	public Animal findByCodAnimal(int codAnimal) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.codAnimal, pet.nome "
					+ "FROM servico AS servico "
					+ "INNER JOIN animal AS pet "
					+ "ON servico.codAnimal = pet.codAnimal "
					+ "WHERE pet.codAnimal =? ", new AnimalRowMapper(), codAnimal);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Animal findByNomeAnimal(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.nome "
					+ "FROM servico AS servico "
					+ "INNER JOIN animal AS pet "
					+ "ON servico.codAnimal = pet.codAnimal "
					+ "WHERE pet.nome =? ", new AnimalRowMapper(), nome);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Animal findByEspecie(String especie) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.especie, pet.nome "
					+ "FROM servico AS servico "
					+ "INNER JOIN animal AS pet "
					+ "ON servico.codAnimal = pet.codAnimal "
					+ "WHERE pet.especie =? ", new AnimalRowMapper(), especie);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Animal findByRaca(String raca) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.raca, pet.nome "
					+ "FROM servico AS servico "
					+ "INNER JOIN animal AS pet "
					+ "ON servico.codAnimal = pet.codAnimal "
					+ "WHERE pet.raca =? ", new AnimalRowMapper(), raca);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public List<Servico> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM servico", new ServicoRowMapper());
		} catch(EmptyResultDataAccessException servico){
			return null;
		}
	}

	@Override
	public int save(Servico codServico) {
		if (codServico.getCodServico() <= 0) { // Inserir
			return jdbcTemplate.update("INSER INTO servico "
					+ "(dadaServico, nome, valor, codAnimal, nome, especie, raca) "
					+ "VALUES (?,?,?,?,?,?,?)",  
					codServico.getDate().getTime(),
					codServico.getNome(), 
					codServico.getValor(),
					codServico.getPet().getCodAnimal(), 
					codServico.getPet().getNome(), 
					codServico.getPet().getEspecie(), 
					codServico.getPet().getRaca());
		} else { // Atualizar
			return jdbcTemplate.update("UPDATE servico "
					+ "dataServico =?, nome =?, valor =?, codAnimal=? , nome =?, "
					+ "especie =?, raca =? WHERE codServico =?",  
					codServico.getDate().getTime(),
					codServico.getNome(), 
					codServico.getValor(),
					codServico.getPet().getCodAnimal(), 
					codServico.getPet().getNome(), 
					codServico.getPet().getEspecie(), 
					codServico.getPet().getRaca());
		}
	}

	@Override
	public int delete(Integer codServico) {
		if (codServico > 0) { // Apagar
			return jdbcTemplate.update ("DELETE FROM servico WHERE codServico = ?", 
					codServico);
		}
		return 0;
	}

	@Override
	public Servico findByCalendario(Date date) {
		Calendar c = Calendar.getInstance();
        Date data = c.getTime();
         
        DateFormat f = DateFormat.getDateInstance(DateFormat.FULL); //Data COmpleta
 
        f = DateFormat.getDateInstance(DateFormat.SHORT);
        System.out.println("Data: "+f.format(data));
		return null;
	}
}
