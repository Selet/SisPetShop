package sis.pet.dao;

import java.util.List;

import sis.pet.vo.Pessoa;

public interface PessoaDAO {
	Pessoa findByNome(String nome);
	Pessoa findByCPF(String cpf);
	Pessoa findByTelefone(String telefone);
	List<Pessoa> list();
}
