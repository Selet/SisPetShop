package sis.pet.dao;

import java.util.List;

import sis.pet.vo.TipoServico;

public interface TipoServicoDAO {
	TipoServico findByCodTipoServico(int codTipoServico);
	TipoServico findByNome(String nome);
	List<TipoServico> list();
}
