package sis.pet.dao;

import java.util.List;

import sis.pet.vo.Cargo;

public interface CargoDAO {
	Cargo findByFuncao(String funcao);
	Cargo findByCodCargo(int codCargo);
	List<Cargo> list();
}
