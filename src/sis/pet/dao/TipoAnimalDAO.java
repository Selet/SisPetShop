package sis.pet.dao;

import java.util.List;

import sis.pet.vo.TipoAnimal;

public interface TipoAnimalDAO {
	TipoAnimal findById(int id);
	TipoAnimal findByEspecie(String especie);
	TipoAnimal findByRaca(String raca);
	List<TipoAnimal> list();
	int save(TipoAnimal tipo);
	int delete(TipoAnimal tipo);
}
