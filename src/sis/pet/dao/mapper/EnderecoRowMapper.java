package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.vo.Endereco;

public class EnderecoRowMapper implements RowMapper<Endereco>{
	
	@Override
	public Endereco mapRow(ResultSet rs, int rowNum) throws SQLException{
		Endereco end = new Endereco();
		end.setLogradouro(rs.getString("logradouro"));
		end.setBairro(rs.getString("bairro"));
		end.setNumCasa(rs.getInt("numCasa"));
		end.setComplemento(rs.getString("complemento"));
		end.setCEP(rs.getString("cep"));
		end.setCidade(rs.getString("cidade"));
		return end;
	}
}
