package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.vo.Cargo;

public class CargoRowMapper implements RowMapper<Cargo>{
	
	@Override
	public Cargo mapRow(ResultSet rs, int numRow) throws SQLException{
		
		Cargo cargo = new Cargo();
		cargo.setFuncao(rs.getString("funcao"));
		cargo.setCodCargo(rs.getInt("codCargo"));
		return cargo;
	}
}
