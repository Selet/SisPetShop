package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.vo.TipoAnimal;

public class TipoAnimalRowMapper implements RowMapper<TipoAnimal>{
	
	@Override
	public TipoAnimal mapRow(ResultSet rs, int rowNum) throws SQLException{
		TipoAnimal tipo = new TipoAnimal();
		tipo.setEspecie(rs.getString("especie"));
		tipo.setRaca(rs.getString("raca"));
		return tipo;
	}
}
