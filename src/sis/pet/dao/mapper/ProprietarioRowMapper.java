package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.dao.AnimalDAO;
import sis.pet.dao.AnimalDAOImpl;
import sis.pet.dao.PessoaDAO;
import sis.pet.dao.PessoaDAOImpl;
import sis.pet.vo.Proprietario;

public class ProprietarioRowMapper implements RowMapper<Proprietario>{
	
	@Override
	public Proprietario mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		Proprietario dono = new Proprietario();
		PessoaDAO pessoa = new PessoaDAOImpl();
		AnimalDAO pet = new AnimalDAOImpl();
		
		dono.setCodProprietario(rs.getInt("codProprietario"));
		dono.setPessoa(pessoa.findByNome(rs.getString("nome")));
		dono.setPessoa(pessoa.findByCPF(rs.getString("cpf")));
		dono.setPessoa(pessoa.findByTelefone(rs.getString("telefone")));
		dono.setPet(pet.findByCodAnimal(rs.getInt("codAnimal")));
		dono.setPet(pet.findByNome(rs.getString("nome")));
		dono.setPet(pet.findByEspecie(rs.getString("especie")));
		dono.setPet(pet.findByRaca(rs.getString("raca")));
		return dono;
	}
}
