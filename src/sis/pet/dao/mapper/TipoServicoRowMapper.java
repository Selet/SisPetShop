package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.vo.TipoServico;

public class TipoServicoRowMapper implements RowMapper<TipoServico>{

	@Override
	public TipoServico mapRow(ResultSet rs, int rowNum) throws SQLException {
		TipoServico tipo = new TipoServico();
		tipo.setCodTipoServico(rs.getInt("codTipoServico"));
		tipo.setNome(rs.getString("nome"));
		return tipo;
	}

}
