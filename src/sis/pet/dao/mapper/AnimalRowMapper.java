package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.vo.Animal;

public class AnimalRowMapper implements RowMapper<Animal>{
	
	@Override
	public Animal mapRow(ResultSet rs, int rowNum) throws SQLException{
		Animal pet = new Animal();
		pet.setCodAnimal(rs.getInt("codAnimal"));
		pet.setNome(rs.getString("nome"));
		pet.setEspecie(rs.getString("especie"));
		pet.setRaca(rs.getString("raca"));		
		return pet;
	}
}