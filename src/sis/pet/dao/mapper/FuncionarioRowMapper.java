package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.dao.CargoDAO;
import sis.pet.dao.CargoDAOImpl;
import sis.pet.dao.PessoaDAO;
import sis.pet.dao.PessoaDAOImpl;
import sis.pet.vo.Funcionario;

public class FuncionarioRowMapper implements RowMapper<Funcionario>{
	
	@Override
	public Funcionario mapRow(ResultSet rs, int rowNum) throws SQLException{
		
		Funcionario peao = new Funcionario();
		PessoaDAO pessoa = new PessoaDAOImpl();
		CargoDAO cargo = new CargoDAOImpl();
		
		peao.setCodFuncionario(rs.getInt("codFuncionario"));
		peao.setSenha(rs.getString("senha"));
		peao.setPessoa(pessoa.findByNome(rs.getString("nome")));
		peao.setPessoa(pessoa.findByCPF(rs.getString("cpf")));
		peao.setPessoa(pessoa.findByTelefone(rs.getString("telefone")));
		peao.setCargo(cargo.findByFuncao(rs.getString("funcao")));
		return peao;
	}
}
