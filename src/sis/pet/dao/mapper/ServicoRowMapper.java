package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.dao.AnimalDAO;
import sis.pet.dao.AnimalDAOImpl;
import sis.pet.vo.Servico;

public class ServicoRowMapper implements RowMapper<Servico>{
	
	@Override
	public Servico mapRow(ResultSet rs, int rowNum) throws SQLException{
		Servico servico = new Servico();
		AnimalDAO pet = new AnimalDAOImpl();
		
		servico.setCodServico(rs.getInt("codServico"));
		servico.setDescricao(rs.getString("descricao"));
		servico.setValor(rs.getInt("valor"));
		servico.setNome(rs.getString("nome"));
		servico.setDate(rs.getTime(rowNum, null));
		servico.setPet(pet.findByCodAnimal(rs.getInt("codAnimal")));
		servico.setPet(pet.findByNome(rs.getString("nome")));
		servico.setPet(pet.findByEspecie(rs.getString("especie")));
		servico.setPet(pet.findByRaca(rs.getString("raca")));
		return servico;
	}
}
