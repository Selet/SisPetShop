package sis.pet.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import sis.pet.vo.Pessoa;

public class PessoaRowMapper implements RowMapper<Pessoa>{

	@Override
	public Pessoa mapRow(ResultSet rs, int numRow) throws SQLException{
		Pessoa pessoa = new Pessoa();
		
		pessoa.setNome(rs.getString("nome"));
		pessoa.setCPF(rs.getString("cpf"));
		pessoa.setTelefone(rs.getString("telefone"));
		return pessoa;
	}
}
