package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.PessoaRowMapper;
import sis.pet.vo.Pessoa;

public class PessoaDAOImpl implements PessoaDAO {

	@Autowired
	public JdbcTemplate jdbcTemplate;
	
	@Override
	public Pessoa findByNome(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"pessoa WHERE nome =?", new PessoaRowMapper(), nome);
		}catch(EmptyResultDataAccessException pessoa) {
			return null;
		}
	}

	@Override
	public Pessoa findByCPF(String cpf) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"pessoa WHERE cpf =?", new PessoaRowMapper(), cpf);
		}catch(EmptyResultDataAccessException pessoa) {
			return null;
		}
	}

	@Override
	public Pessoa findByTelefone(String telefone) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"pessoa WHERE telefone =?", new PessoaRowMapper(), telefone);
		}catch(EmptyResultDataAccessException pessoa) {
			return null;
		}
	}

	@Override
	public List<Pessoa> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM pessoa ", 
					new PessoaRowMapper());
		} catch (EmptyResultDataAccessException pessoa) {
			return null;
		}
	}
}
