package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.CargoRowMapper;
import sis.pet.dao.mapper.FuncionarioRowMapper;
import sis.pet.dao.mapper.PessoaRowMapper;
import sis.pet.vo.Cargo;
import sis.pet.vo.Funcionario;
import sis.pet.vo.Pessoa;

public class FuncionarioDAOImpl implements FuncionarioDAO{

	@Autowired
	public JdbcTemplate jdbcTemplate;
	
	@Override
	public Funcionario findByCodFuncionario(int codFuncionario) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " +
		"funcionario WHERE codFuncionario =?", new FuncionarioRowMapper(), codFuncionario);
		}catch(EmptyResultDataAccessException peao){
			return null;
		}
	}

	@Override
	public Pessoa findByCodPessoa(int codPessoa) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.codPessoa, pessoa.nome "
					+ "FROM funcionario AS peao "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON peao.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.codPessoa =? ", new PessoaRowMapper(), codPessoa);
		} catch(EmptyResultDataAccessException peao){
			return null;
		}
	}

	@Override
	public Pessoa findByNomePessoa(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.codPessoa, pessoa.nome "
					+ "FROM funcionario AS peao "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON peao.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.nome =? ", new PessoaRowMapper(), nome);
		} catch(EmptyResultDataAccessException peao){
			return null;
		}
	}

	@Override
	public Pessoa findByCPFPessoa(int cpf) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.codPessoa, pessoa.nome "
					+ "FROM funcionario AS peao "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON peao.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.cpf =? ", new PessoaRowMapper(), cpf);
		} catch(EmptyResultDataAccessException peao){
			return null;
		}
	}

	@Override
	public Pessoa findByTelefonePessoa(int telefone) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.nome, pessoa.telefone "
					+ "FROM funcionario AS peao "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON peao.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.telefone =? ", new PessoaRowMapper(), telefone);
		} catch(EmptyResultDataAccessException peao){
			return null;
		}
	}

	@Override
	public Cargo findByCodCargo(int codCargo) {
		try {
			return jdbcTemplate.queryForObject("SELECT  cargo.codCargo, pessoa.nome " 
					+ "FROM funcionario AS peao " 
					+ "INNER JOIN pessoa AS pessoa " 
					+ "ON peao.codPessoa = pessoa.codPessoa " 
					+ "INNER JOIN cargo AS cargo " 
					+ "ON peao.codCargo = cargo.codCargo " 
					+ "WHERE cargo.codCargo = ?;", new CargoRowMapper(), codCargo);
		}catch(EmptyResultDataAccessException peao) {
			return null;
		}
	}

	@Override
	public Cargo findByFuncao(String funcao) {
		try {
			return jdbcTemplate.queryForObject("SELECT cargo.funcao, pessoa.nome, " 
					+ "FROM funcionario AS peao " 
					+ "INNER JOIN pessoa AS pessoa " 
					+ "ON peao.codPessoa = pessoa.codPessoa " 
					+ "INNER JOIN cargo AS cargo " 
					+ "ON peao.codCargo = cargo.codCargo " 
					+ "WHERE cargo.codCargo = ?;", new CargoRowMapper(), funcao);
		}catch(EmptyResultDataAccessException peao) {
			return null;
		}
	}

	@Override
	public Funcionario findBySenha(String senha) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.nome, peao.senha "
					+ "FROM funcionario AS peao "
					+ "INNER JOIN pessoa AS pessoa "
					+ "WHERE peao.senha =? ", new FuncionarioRowMapper(), senha);
		} catch(EmptyResultDataAccessException peao){
			return null;
		}
	}

	@Override
	public List<Funcionario> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM funcionario ", 
					new FuncionarioRowMapper());
		} catch (EmptyResultDataAccessException peao) {
			return null;
		}
	}

	@Override
	public int save(Funcionario codFuncionario) {
		if (codFuncionario.getCodFuncionario() <= 0) { // Inserir
			return jdbcTemplate.update("INSER INTO proprietario "
					+ "(cpf, nome, senha, telefone, cargo) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",  
					codFuncionario.getPessoa().getCPF(),
					codFuncionario.getPessoa().getNome(), 
					codFuncionario.getSenha(),
					codFuncionario.getPessoa().getTelefone(),
					codFuncionario.getCargo().getFuncao());
		} else { // Atualizar
			return jdbcTemplate.update("UPDATE funcionario "
					+ "cpf =?, nome =?, senha =?, telefone =?, cargo =? "
					+ "WHERE codFuncionario =?",
					codFuncionario.getPessoa().getCPF(),
					codFuncionario.getPessoa().getNome(), 
					codFuncionario.getSenha(),
					codFuncionario.getPessoa().getTelefone(),
					codFuncionario.getCargo().getFuncao(),
					codFuncionario.getCodFuncionario());
		}
	}

	@Override
	public int delete(Integer codFuncionario) {
		if (codFuncionario > 0) { // Apagar
			return jdbcTemplate.update ("DELETE FROM funcionario WHERE codFuncionario = ?", 
					codFuncionario);
		}
		return 0;
	}

}
