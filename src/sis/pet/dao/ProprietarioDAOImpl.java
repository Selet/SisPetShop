package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.AnimalRowMapper;
import sis.pet.dao.mapper.ProprietarioRowMapper;
import sis.pet.vo.Animal;
import sis.pet.vo.Proprietario;

public class ProprietarioDAOImpl implements ProprietarioDAO {

	@Autowired
	public JdbcTemplate jdbcTemplate;
	
	@Override
	public Proprietario findByCodProprietario(int codProprietario) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"proprietario WHERE codProprietario =?", new ProprietarioRowMapper(), codProprietario);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Proprietario findByCodPessoa(int codPessoa) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.codPessoa, pessoa.nome "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON dono.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.codPessoa =? ", new ProprietarioRowMapper(), codPessoa);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Proprietario findByNomePessoa(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.nome "
					+ "FROM proprietario AS dono " 
					+ "INNER JOIN pessoa AS pessoa " 
					+ "ON dono.codPessoa = pessoa.codPessoa " 
					+ "WHERE pessoa.nome =? ", new ProprietarioRowMapper(), nome);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Proprietario findByCPFPessoa(int cpf) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.cpf "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON dono.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.cpf = ? ", new ProprietarioRowMapper(), cpf);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Proprietario findByTelefonePessoa(int telefone) {
		try {
			return jdbcTemplate.queryForObject("SELECT pessoa.nome, pessoa.telefone "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN pessoa AS pessoa "
					+ "ON dono.codPessoa = pessoa.codPessoa "
					+ "WHERE pessoa.telefone =? ", new ProprietarioRowMapper(), telefone);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}
	
	@Override
	public Animal findByCodAnimal(int codAnimal) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.codAnimal, pet.nome "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN animal AS pet "
					+ "ON dono.codAnimal = pet.codAnimal "
					+ "WHERE pet.codAnimal =? ", new AnimalRowMapper(), codAnimal);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Animal findByNomeAnimal(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.nome "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN animal AS pet "
					+ "ON dono.codAnimal = pet.codAnimal "
					+ "WHERE pet.nome =? ", new AnimalRowMapper(), nome);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Animal findByEspecie(String especie) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.especie, pet.nome "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN animal AS pet "
					+ "ON dono.codAnimal = pet.codAnimal "
					+ "WHERE pet.especie =? ", new AnimalRowMapper(), especie);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public Animal findByRaca(String raca) {
		try {
			return jdbcTemplate.queryForObject("SELECT pet.raca, pet.nome "
					+ "FROM proprietario AS dono "
					+ "INNER JOIN animal AS pet "
					+ "ON dono.codAnimal = pet.codAnimal "
					+ "WHERE pet.raca =? ", new AnimalRowMapper(), raca);
		} catch(EmptyResultDataAccessException dono){
			return null;
		}
	}

	@Override
	public List<Proprietario> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM animal ", 
					new ProprietarioRowMapper());
		} catch (EmptyResultDataAccessException proprietario) {
			return null;
		}
	}

	@Override
	public int save(Proprietario codProprietario) {
		if (codProprietario.getCodProprietario() <= 0) { // Inserir
			return jdbcTemplate.update("INSER INTO proprietario "
					+ "(cpf, nome, telefone, logradouro, numCasa, bairro, "
					+ "cep, complemento, cidade) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",  
					codProprietario.getPessoa().getCPF(),
					codProprietario.getPessoa().getNome(), 
					codProprietario.getPessoa().getTelefone(),
					codProprietario.getCodEndereco().getLogradouro(), 
					codProprietario.getCodEndereco().getNumCasa(), 
					codProprietario.getCodEndereco().getBairro(), 
					codProprietario.getCodEndereco().getCEP(), 
					codProprietario.getCodEndereco().getComplemento(), 
					codProprietario.getCodEndereco().getCidade());
		} else { // Atualizar
			return jdbcTemplate.update("UPDATE proprietario "
					+ "cpf =?, nome =?, telefone =?, logradouro=? , numCasa =?, bairro =?, "
					+ "cep =?, complemento =?, cidade =? WHERE codProprietario =?",  
					codProprietario.getPessoa().getCPF(),
					codProprietario.getPessoa().getNome(), 
					codProprietario.getPessoa().getTelefone(),
					codProprietario.getCodEndereco().getLogradouro(), 
					codProprietario.getCodEndereco().getNumCasa(), 
					codProprietario.getCodEndereco().getBairro(), 
					codProprietario.getCodEndereco().getCEP(), 
					codProprietario.getCodEndereco().getComplemento(), 
					codProprietario.getCodEndereco().getCidade(), 
					codProprietario.getCodProprietario());
		}
	}

	@Override
	public int delete(Integer codProprietario) {
		if (codProprietario > 0) { // Apagar
			return jdbcTemplate.update ("DELETE FROM proprietario WHERE codProprietario = ?", 
					codProprietario);
		}
		return 0;
	}

}
