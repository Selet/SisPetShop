package sis.pet.dao;

import java.util.List;

import sis.pet.vo.Animal;
import sis.pet.vo.Proprietario;

public interface ProprietarioDAO {
	Proprietario findByCodProprietario(int codProprietario);
	Proprietario findByCodPessoa(int codPessoa);
	Proprietario findByNomePessoa(String nome);
	Proprietario findByCPFPessoa(int cpf);
	Proprietario findByTelefonePessoa(int telefone);
	Animal findByCodAnimal(int codAnimal);
	Animal findByNomeAnimal(String nome);
	Animal findByEspecie(String especie);
	Animal findByRaca(String raca);
	List<Proprietario> list();
	int save(Proprietario codProprietario);
	int delete(Integer codProprietario);
}
