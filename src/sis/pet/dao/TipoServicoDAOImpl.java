package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.TipoServicoRowMapper;
import sis.pet.vo.TipoServico;

public class TipoServicoDAOImpl implements TipoServicoDAO {

	@Autowired
	public JdbcTemplate jdbcTemplate;

	@Override
	public TipoServico findByCodTipoServico(int codTipoServico) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + "tipoServico WHERE codTipoServico =?",
					new TipoServicoRowMapper(), codTipoServico);
		} catch (EmptyResultDataAccessException tipo) {
			return null;
		}
	}

	@Override
	public TipoServico findByNome(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + "tipoServico WHERE nome =?",
					new TipoServicoRowMapper(), nome);
		} catch (EmptyResultDataAccessException tipo) {
			return null;
		}
	}

	@Override
	public List<TipoServico> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM tipoServico", new TipoServicoRowMapper());
		} catch (EmptyResultDataAccessException tipo) {
			return null;
		}
	}
}
