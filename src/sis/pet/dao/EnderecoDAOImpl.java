package sis.pet.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.EnderecoRowMapper;
import sis.pet.vo.Endereco;

public class EnderecoDAOImpl implements EnderecoDAO{

	
	@Autowired
	public JdbcTemplate jdbcTemplate;
	
	@Override
	public Endereco findByCodEndereco(int codEndereco) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE codEndereco =?", new EnderecoRowMapper(), codEndereco);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}
	
	@Override
	public Endereco findByLogradouro(String logradouro) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE logradouro =?", new EnderecoRowMapper(), logradouro);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}

	@Override
	public Endereco findByNumCasa(int numCasa) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE numCasa =?", new EnderecoRowMapper(), numCasa);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}

	@Override
	public Endereco findByBairro(String bairro) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE bairro =?", new EnderecoRowMapper(), bairro);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}

	@Override
	public Endereco findByComplemento(String complemento) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE complemento =?", new EnderecoRowMapper(), complemento);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}
	
	@Override
	public Endereco findByCEP(String cep) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE cep =?", new EnderecoRowMapper(), cep);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}

	@Override
	public Endereco findByCidade(String cidade) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"endereco WHERE cidade =?", new EnderecoRowMapper(), cidade);
		}catch(EmptyResultDataAccessException endereco) {
			return null;
		}
	}
}
