package sis.pet.dao;

import java.util.Date;
import java.util.List;

import sis.pet.vo.Animal;
import sis.pet.vo.Servico;

public interface ServicoDAO {
	Servico findByCodServico(int codServico);
	Servico findByTipoServico(String nome);
	Servico findByDescricao(String descricao);
	Servico findByValor(double valor);
	Servico findByCalendario(Date date);
	Animal findByCodAnimal(int codAnimal);
	Animal findByNomeAnimal(String nome);
	Animal findByEspecie(String especie);
	Animal findByRaca(String raca);
	List<Servico> list();
	int save(Servico codServico);
	int delete(Integer codServico);
	
}
