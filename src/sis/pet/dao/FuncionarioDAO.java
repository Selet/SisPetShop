package sis.pet.dao;

import java.util.List;

import sis.pet.vo.Cargo;
import sis.pet.vo.Funcionario;
import sis.pet.vo.Pessoa;

public interface FuncionarioDAO {
	Funcionario findByCodFuncionario(int codFuncionario);
	Pessoa findByCodPessoa(int codPessoa);
	Pessoa findByNomePessoa(String nome);
	Pessoa findByCPFPessoa(int cpf);
	Pessoa findByTelefonePessoa(int telefone);
	Cargo findByFuncao(String funcao);
	Cargo findByCodCargo(int codCargo);
	Funcionario findBySenha(String senha);
	List<Funcionario> list();
	int save(Funcionario codFuncionario);
	int delete(Integer codFuncionario);
}
