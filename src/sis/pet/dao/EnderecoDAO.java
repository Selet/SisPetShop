package sis.pet.dao;

import sis.pet.vo.Endereco;

public interface EnderecoDAO {
	Endereco findByCodEndereco(int codEndereco);
	Endereco findByLogradouro(String logradouro);
	Endereco findByNumCasa(int numCasa);
	Endereco findByBairro(String bairro);
	Endereco findByCEP(String cep);
	Endereco findByComplemento(String complemento);
	Endereco findByCidade(String cidade);
}
