package sis.pet.dao;

import java.util.List;

import sis.pet.vo.Animal;

public interface AnimalDAO {
	Animal findByCodAnimal(int codAnimal);
	Animal findByNome(String nome);
	Animal findByEspecie(String especie);
	Animal findByRaca(String raca);
	List<Animal> list();
	int save(Animal codAnimal);
	int delete(Integer codAnimal);
}
