package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import sis.pet.dao.mapper.CargoRowMapper;
import sis.pet.vo.Cargo;

public class CargoDAOImpl implements CargoDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;


	@Override
	public Cargo findByCodCargo(int codCargo) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"cargo WHERE codCargo =?", new CargoRowMapper(), codCargo);
		}catch(EmptyResultDataAccessException cargo) {
			return null;
		}
	}
	
	@Override
	public Cargo findByFuncao(String funcao) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"cargo WHERE funcao =?", new CargoRowMapper(), funcao);
		}catch(EmptyResultDataAccessException cargo) {
			return null;
		}
	}

	@Override
	public List<Cargo> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM cargo ", 
					new CargoRowMapper());
		} catch (EmptyResultDataAccessException cargo) {
			return null;
		}
	}
} 
