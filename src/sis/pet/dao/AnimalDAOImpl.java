package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import sis.pet.dao.mapper.AnimalRowMapper;
import sis.pet.vo.Animal;

@Repository
public class AnimalDAOImpl implements AnimalDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Animal findByCodAnimal(int codAnimal) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"animal WHERE codAnimal =?", new AnimalRowMapper(), codAnimal);
		}catch(EmptyResultDataAccessException pet) {
			return null;
		}
	}

	@Override
	public Animal findByNome(String nome) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"animal WHERE nome =?", new AnimalRowMapper(), nome);
		}catch(EmptyResultDataAccessException pet){
			return null;
		}
	}

	@Override
	public Animal findByEspecie(String especie) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"animal WHERE especie =?", new AnimalRowMapper(), especie);
		} catch(EmptyResultDataAccessException pet){
			return null;
		}
	}

	@Override
	public Animal findByRaca(String raca) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM " + 
		"animal WHERE raca =?", new AnimalRowMapper(), raca);
		} catch(EmptyResultDataAccessException pet){
			return null;
		}
	}
	
	@Override
	public List<Animal> list() {
		try {
			return jdbcTemplate.query("SELECT * FROM animal ORDER BY animal", 
					new AnimalRowMapper());
		} catch (EmptyResultDataAccessException pet) {
			return null;
		}
	}

	@Override
	public int save(Animal animal) {
		if (animal.getCodAnimal() <= 0) { // Inserir
			return jdbcTemplate.update("INSERT INTO animal(nome, especie, raca) "
					+ "values (?,?,?)", animal.getNome(), 
					animal.getEspecie(), animal.getRaca());
		} else { // Atualizar
			return jdbcTemplate.update("UPDATE animal "
					+ "set nome =?, especie =?, raca =? WHERE codAnimal =?", 
					animal.getNome(), animal.getEspecie(), 
					animal.getRaca(), animal.getCodAnimal());
		}
	}


	@Override
	public int delete(Integer codAnimal) {
		if (codAnimal > 0) { // Apagar
			return jdbcTemplate.update ("DELETE FROM animal WHERE codAnimal = ?", 
					codAnimal);
		}
		return 0;
	}

}
