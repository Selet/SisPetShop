package sis.pet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import sis.pet.dao.mapper.TipoAnimalRowMapper;
import sis.pet.vo.TipoAnimal;

@Repository
public class TipoAnimalDAOImpl implements TipoAnimalDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public TipoAnimal findById(int id) {
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM tipo_animal WHERE id = ?", new TipoAnimalRowMapper(),
					id);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public TipoAnimal findByEspecie(String especie) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoAnimal findByRaca(String raca) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TipoAnimal> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(TipoAnimal tipo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(TipoAnimal tipo) {
		// TODO Auto-generated method stub
		return 0;
	}

}
